package org.sas.configurations;

import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("jwt")
public class JwtConfiguration {

    @Value("${JWT_SECRET}")
    private String secret;

    private SignatureAlgorithm algorithm;

    private int sensorTokenTimeInMinutes;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public SignatureAlgorithm getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(SignatureAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    public int getSensorTokenTimeInMinutes() {
        return sensorTokenTimeInMinutes;
    }

    public void setSensorTokenTimeInMinutes(int sensorTokenTimeInMinutes) {
        this.sensorTokenTimeInMinutes = sensorTokenTimeInMinutes;
    }
}
