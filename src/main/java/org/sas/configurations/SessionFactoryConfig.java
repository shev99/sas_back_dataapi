package org.sas.configurations;

import java.util.LinkedHashMap;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@org.springframework.context.annotation.Configuration
public class SessionFactoryConfig {
    private static final LinkedHashMap<String, String> envMap = new LinkedHashMap<>();

    static {
        final String dbUrl = "jdbc:postgresql://" + System.getenv("PG_DB_HOST") + ":" +
                System.getenv("PG_DB_PORT") + "/" + System.getenv("PG_DB_NAME");

        envMap.put("connection.url", dbUrl);
        envMap.put("connection.username", System.getenv("PG_USER_NAME"));
        envMap.put("connection.password", System.getenv("PG_USER_PASS"));
    }

    private SessionFactory sessionFactory;

    @Bean(name="entityManagerFactory")
    public SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = setDBProperties();
        }
        return sessionFactory;
    }

    private SessionFactory setDBProperties() {
        Configuration cfg = new Configuration().configure();
        for(Map.Entry<String, String> entry : envMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            cfg.setProperty(key, value);
            cfg.setProperty("hibernate." + key, value);
        }
        return cfg.buildSessionFactory();
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(getSessionFactory());
        return transactionManager;
    }
}
