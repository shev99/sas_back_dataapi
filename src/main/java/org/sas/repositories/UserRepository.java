package org.sas.repositories;

import org.sas.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Override
    @Nonnull
    Optional<User> findById(@Nonnull Integer userId);

    @Nonnull
    @Query("select u from User u where u.accessToken = ?1")
    Optional<User> findByAccessToken(@Nonnull String accessToken);

    @Nonnull
    @Query("select u from User u where u.sensorToken = ?1")
    Optional<User> findBySensorToken(@Nonnull String sensorToken);

    @Override
    @Nonnull
    <S extends User> S save(@Nonnull S s);
}
