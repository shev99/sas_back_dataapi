package org.sas.repositories;

import org.sas.entities.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import java.util.Optional;

@Repository
public interface SensorRepository extends JpaRepository<Sensor, Integer> {
    @Override
    @Nonnull
    Optional<Sensor> findById(@Nonnull Integer sensorId);

    @Query("select s from Sensor s join fetch s.user where s.id = ?1")
    Optional<Sensor> findWithJoinFetch(@Nonnull Integer sensorId);
}
