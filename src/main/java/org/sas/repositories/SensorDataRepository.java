package org.sas.repositories;

import org.sas.entities.Sensor;
import org.sas.entities.SensorData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface SensorDataRepository extends JpaRepository<SensorData, Integer> {
    @Override
    @Nonnull
    <S extends SensorData> S save(@Nonnull S s);

    List<SensorData> findBySensorAndMeasurementTimeAfterOrderByMeasurementTime(
            Sensor sensor, @Nonnull Timestamp start);

    List<SensorData> findBySensorAndMeasurementTimeBeforeOrderByMeasurementTime(
            Sensor sensor, @Nonnull Timestamp end);

    List<SensorData> findBySensorAndMeasurementTimeBetweenOrderByMeasurementTime(
            Sensor sensor, @Nonnull Timestamp start, @Nonnull Timestamp end);
}
