package org.sas.services;

import javax.annotation.Nonnull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import io.jsonwebtoken.Jwts;
import org.sas.configurations.JwtConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JwtService {
    private final JwtConfiguration jwtConfiguration;

    @Autowired
    public JwtService(JwtConfiguration jwtConfiguration) {
        this.jwtConfiguration = jwtConfiguration;
    }

    public String generateSensorToken(String userLogin) {
        return generateToken(userLogin, jwtConfiguration.getSensorTokenTimeInMinutes());
    }

    @Nonnull
    private String generateToken(String userLogin, int tokenLifetimeInMinutes) {
        if (tokenLifetimeInMinutes <= 0) {
            return Jwts.builder()
                    .setSubject(userLogin)
                    .setIssuedAt(Date.from(Instant.now()))
                    .signWith(jwtConfiguration.getAlgorithm(), jwtConfiguration.getSecret())
                    .compact();
        }

        Date date = Date.from(LocalDateTime.now()
                        .plusMinutes(tokenLifetimeInMinutes)
                        .atZone(ZoneId.systemDefault())
                        .toInstant());

        return Jwts.builder()
                .setSubject(userLogin)
                .setExpiration(date)
                .signWith(jwtConfiguration.getAlgorithm(), jwtConfiguration.getSecret())
                .compact();
    }
}
