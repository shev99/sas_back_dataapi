package org.sas.enums;

public enum ErrorCode {

    USER_NOT_FOUND(7, "Invalid access token"),
    SENSOR_NOT_FOUND(14, "Invalid sensor id"),
    SENSOR_TOKEN_NOT_FOUND(18, "Invalid sensor token"),
    USER_IS_BANNED(19, "Your account has been banned"),
    REQUESTS_LIMIT_EXCEEDED(20,
            "The limit of requests for this type of user is exceeded"),
    MISSING_SENSOR_ID(21, "The sensorId field is missing in the request body"),
    MISSING_SENSOR_DATA_TIMESTAMP(22,
            "The measurementTime field is missing in the request body"),
    MISSING_SENSOR_DATA_VALUE(23,
            "The sensorId field is missing in the request body");

    private final int code;
    private final String message;

    ErrorCode(int code, String msg) {
        this.code = code;
        message = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
