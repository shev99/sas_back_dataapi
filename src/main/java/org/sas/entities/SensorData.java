package org.sas.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "sensor_data", schema = "public", catalog = "sas")
public class SensorData {
    private Integer id;
    private Sensor sensor;
    private Float value;
    private Timestamp measurementTime;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "sensor", nullable = false)
    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    @Basic
    @Column(name = "value", nullable = false)
    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    @Basic
    @Column(name = "measurement_time", nullable = false)
    public Timestamp getMeasurementTime() {
        return measurementTime;
    }

    public void setMeasurementTime(Timestamp measurementTime) {
        this.measurementTime = measurementTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SensorData that = (SensorData) o;

        if (!Objects.equals(id, that.id)) return false;
        if (!Objects.equals(sensor, that.sensor)) return false;
        if (!Objects.equals(value, that.value)) return false;
        return Objects.equals(measurementTime, that.measurementTime);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (sensor != null ? sensor.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (measurementTime != null ? measurementTime.hashCode() : 0);
        return result;
    }
}
