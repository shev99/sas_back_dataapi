package org.sas.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "users", schema = "public", catalog = "sas")
public class User {
    private Integer id;
    private String login;
    private String email;
    private String password;
    private Boolean isConfirmed;
    private String accessToken;
    private String refreshToken;
    private String sensorToken;
    private Short utcTimezone;
    private Integer housesNumber;
    private Integer sensorsNumber;
    private Integer requestsNumber;
    private UserType type;
    private Boolean isBanned;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "login", nullable = false, length = 64)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "email", nullable = false, length = 64)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 64)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "is_confirmed", nullable = false)
    public Boolean getIsConfirmed() {
        return isConfirmed;
    }

    public void setIsConfirmed(Boolean confirmed) {
        this.isConfirmed = confirmed;
    }

    @Basic
    @Column(name = "access_token", length = -1)
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Basic
    @Column(name = "refresh_token", length = -1)
    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    @Basic
    @Column(name = "sensor_token", length = -1)
    public String getSensorToken() {
        return sensorToken;
    }

    public void setSensorToken(String sensorToken) {
        this.sensorToken = sensorToken;
    }

    @Basic
    @Column(name = "utc_timezone")
    public Short getUtcTimezone() {
        return utcTimezone;
    }

    public void setUtcTimezone(Short utcTimezone) {
        this.utcTimezone = utcTimezone;
    }

    @Basic
    @Column(name = "houses_number", nullable = false)
    public Integer getHousesNumber() {
        return housesNumber;
    }

    public void setHousesNumber(Integer housesNumber) {
        this.housesNumber = housesNumber;
    }

    @Basic
    @Column(name = "sensors_number", nullable = false)
    public Integer getSensorsNumber() {
        return sensorsNumber;
    }

    public void setSensorsNumber(Integer sensorsNumber) {
        this.sensorsNumber = sensorsNumber;
    }

    @Basic
    @Column(name = "requests_number", nullable = false)
    public Integer getRequestsNumber() {
        return requestsNumber;
    }

    public void setRequestsNumber(Integer requestsNumber) {
        this.requestsNumber = requestsNumber;
    }

    @Basic
    @Column(name = "is_banned", nullable = false)
    public Boolean getIsBanned() {
        return isBanned;
    }

    public void setIsBanned(Boolean isBanned) {
        this.isBanned= isBanned;
    }

    @ManyToOne
    @JoinColumn(name = "type", nullable = false)
    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!Objects.equals(id, user.id)) return false;
        if (!Objects.equals(login, user.login)) return false;
        if (!Objects.equals(email, user.email)) return false;
        if (!Objects.equals(password, user.password)) return false;
        if (!Objects.equals(accessToken, user.accessToken)) return false;
        if (!Objects.equals(refreshToken, user.refreshToken)) return false;
        if (!Objects.equals(sensorToken, user.sensorToken)) return false;
        if (!Objects.equals(utcTimezone, user.utcTimezone)) return false;
        if (!Objects.equals(housesNumber, user.housesNumber)) return false;
        if (!Objects.equals(sensorsNumber, user.sensorsNumber)) return false;
        if (!Objects.equals(type, user.type)) return false;
        if (!Objects.equals(isBanned, user.isBanned)) return false;
        if (!Objects.equals(isConfirmed, user.isConfirmed )) return false;
        return Objects.equals(requestsNumber, user.requestsNumber);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (accessToken != null ? accessToken.hashCode() : 0);
        result = 31 * result + (refreshToken != null ? refreshToken.hashCode() : 0);
        result = 31 * result + (sensorToken != null ? sensorToken.hashCode() : 0);
        result = 31 * result + (utcTimezone != null ? utcTimezone.hashCode() : 0);
        result = 31 * result + (housesNumber != null ? housesNumber.hashCode() : 0);
        result = 31 * result + (sensorsNumber != null ? sensorsNumber.hashCode() : 0);
        result = 31 * result + (requestsNumber != null ? requestsNumber.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (isBanned != null ? isBanned.hashCode() : 0);
        result = 31 * result + (isConfirmed != null ? isConfirmed.hashCode() : 0);
        return result;
    }
}
