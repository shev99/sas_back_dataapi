package org.sas.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_types", schema = "public", catalog = "sas")
public class UserType {
    private Integer id;
    private String name;
    private Integer sensorsNumber;
    private Integer housesNumber;
    private Integer requestsNumber;
    private Integer requestsReserveNumber;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "sensors_number", nullable = false)
    public Integer getSensorsNumber() {
        return sensorsNumber;
    }

    public void setSensorsNumber(Integer sensorsNumber) {
        this.sensorsNumber = sensorsNumber;
    }

    @Basic
    @Column(name = "houses_number", nullable = false)
    public Integer getHousesNumber() {
        return housesNumber;
    }

    public void setHousesNumber(Integer housesNumber) {
        this.housesNumber = housesNumber;
    }

    @Basic
    @Column(name = "requests_number", nullable = false)
    public Integer getRequestsNumber() {
        return requestsNumber;
    }

    public void setRequestsNumber(Integer requestsNumber) {
        this.requestsNumber = requestsNumber;
    }

    @Basic
    @Column(name = "requests_reserve_number", nullable = false)
    public Integer getRequestsReserveNumber() {
        return requestsReserveNumber;
    }

    public void setRequestsReserveNumber(Integer requestsReserveNumber) {
        this.requestsReserveNumber = requestsReserveNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserType userType = (UserType) o;

        if (!Objects.equals(id, userType.id)) return false;
        if (!Objects.equals(name, userType.name)) return false;
        if (!Objects.equals(sensorsNumber, userType.sensorsNumber)) return false;
        if (!Objects.equals(housesNumber, userType.housesNumber)) return false;
        if (!Objects.equals(requestsNumber, userType.requestsNumber)) return false;
        return Objects.equals(requestsReserveNumber, userType.requestsReserveNumber);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (sensorsNumber != null ? sensorsNumber.hashCode() : 0);
        result = 31 * result + (housesNumber != null ? housesNumber.hashCode() : 0);
        result = 31 * result + (requestsNumber != null ? requestsNumber.hashCode() : 0);
        result = 31 * result + (requestsReserveNumber != null ? requestsReserveNumber.hashCode() : 0);
        return result;
    }
}
