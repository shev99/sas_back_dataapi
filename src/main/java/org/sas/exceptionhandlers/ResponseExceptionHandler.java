package org.sas.exceptionhandlers;

import org.sas.dto.ErrorDTO;
import org.sas.exceptions.AbstractResponseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ResponseExceptionHandler {
    @ExceptionHandler(AbstractResponseException.class)
    public ResponseEntity<ErrorDTO> handleResponseException(AbstractResponseException exception) {
        ResponseStatus responseStatus = exception.getClass().getAnnotation(ResponseStatus.class);
        HttpStatus status = responseStatus == null
                ? HttpStatus.BAD_REQUEST : responseStatus.value();
        return new ResponseEntity<>(new ErrorDTO(status.value(), exception.getMessage()), status);
    }
}
