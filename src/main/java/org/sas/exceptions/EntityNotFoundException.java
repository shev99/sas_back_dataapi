package org.sas.exceptions;

import org.sas.enums.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends AbstractResponseException {

    public EntityNotFoundException(ErrorCode errorCode) {
        super(errorCode.getCode(), errorCode.getMessage());
    }
}
