package org.sas.exceptions;

import org.sas.enums.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class NoSuchPermissionException extends AbstractResponseException {

    public NoSuchPermissionException(ErrorCode errorCode) {
        super(errorCode.getCode(), errorCode.getMessage());
    }
}
