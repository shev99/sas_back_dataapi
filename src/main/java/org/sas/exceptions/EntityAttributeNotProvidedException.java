package org.sas.exceptions;

import org.sas.enums.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EntityAttributeNotProvidedException extends AbstractResponseException {

    public EntityAttributeNotProvidedException(ErrorCode errorCode) {
        super(errorCode.getCode(), errorCode.getMessage());
    }
}
