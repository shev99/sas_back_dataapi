package org.sas.exceptions;

import org.sas.enums.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class UserIsBannedException extends AbstractResponseException {

    public UserIsBannedException(ErrorCode errorCode) {
        super(errorCode.getCode(), errorCode.getMessage());
    }
}
