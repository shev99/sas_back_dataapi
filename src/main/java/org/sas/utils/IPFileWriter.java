package org.sas.utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;

public class IPFileWriter implements AutoCloseable {
    private final BufferedWriter bufferedWriter;

    public IPFileWriter(String fileName) throws InvalidPathException, IOException {
        Path path = Paths.get("./src/main/resources/" + fileName);
        bufferedWriter = Files.newBufferedWriter(path, StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.APPEND);
    }

    public IPFileWriter writeIP(String ipAddress) throws IOException {
        bufferedWriter.write(ipAddress);
        return this;
    }

    @Override
    public void close() throws Exception {
        bufferedWriter.close();
    }
}
