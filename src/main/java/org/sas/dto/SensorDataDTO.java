package org.sas.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.sql.Timestamp;

@JsonSerialize
@JsonDeserialize
public class SensorDataDTO {
    private Integer id;

    private Integer sensorId;

    @JsonFormat(pattern="dd.MM.yyyy HH:mm:ss")
    private Timestamp measurementTime;

    private Float value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSensorId() {
        return sensorId;
    }

    public void setSensorId(Integer sensorId) {
        this.sensorId = sensorId;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Timestamp getMeasurementTime() {
        return measurementTime;
    }

    public void setMeasurementTime(Timestamp measurementTime) {
        this.measurementTime = measurementTime;
    }
}
