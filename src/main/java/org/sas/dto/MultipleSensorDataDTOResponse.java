package org.sas.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class MultipleSensorDataDTOResponse {
    private ShortenedSensorDataDTO[] result;

    public ShortenedSensorDataDTO[] getResult() {
        return result;
    }

    public MultipleSensorDataDTOResponse setResult(ShortenedSensorDataDTO[] result) {
        this.result = result;
        return this;
    }
}
