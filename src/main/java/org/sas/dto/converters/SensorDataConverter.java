package org.sas.dto.converters;

import org.sas.dto.SensorDataDTO;
import org.sas.dto.ShortenedSensorDataDTO;
import org.sas.entities.Sensor;
import org.sas.entities.SensorData;

public class SensorDataConverter {
    private SensorDataConverter() {}

    public static SensorData toData(SensorDataDTO sensorDataDTO, Sensor sensor) {
        SensorData sensorData = new SensorData();

        sensorData.setSensor(sensor);
        sensorData.setValue(sensorDataDTO.getValue());
        sensorData.setMeasurementTime(sensorDataDTO.getMeasurementTime());

        return sensorData;
    }

    public static SensorDataDTO toSensorDataDTO(SensorData sensorData) {
        SensorDataDTO sensorDataDTO = new SensorDataDTO();

        sensorDataDTO.setId(sensorData.getId());
        sensorDataDTO.setSensorId(sensorData.getSensor().getId());
        sensorDataDTO.setValue(sensorData.getValue());
        sensorDataDTO.setMeasurementTime(sensorData.getMeasurementTime());

        return sensorDataDTO;
    }

    public static ShortenedSensorDataDTO toShortenedSensorDataDTO(SensorData sensorData) {
        ShortenedSensorDataDTO sensorDataDTO = new ShortenedSensorDataDTO();

        sensorDataDTO.setDate(sensorData.getMeasurementTime());
        sensorDataDTO.setValue(sensorData.getValue());

        return sensorDataDTO;
    }
}
