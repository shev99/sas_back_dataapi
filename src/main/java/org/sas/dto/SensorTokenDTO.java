package org.sas.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class SensorTokenDTO {
    private String sensorToken;

    public String getSensorToken() {
        return sensorToken;
    }

    public SensorTokenDTO setSensorToken(String sensorToken) {
        this.sensorToken = sensorToken;
        return this;
    }
}
