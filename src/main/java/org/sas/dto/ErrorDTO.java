package org.sas.dto;

public record ErrorDTO(int code, String message) {

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
