package org.sas.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class SensorDataDTOResponse {
    private SensorDataDTO result;

    public SensorDataDTOResponse setResult(SensorDataDTO result) {
        this.result = result;
        return this;
    }

    public SensorDataDTO getResult() {
        return result;
    }
}
