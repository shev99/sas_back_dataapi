package org.sas.controllers;

import org.sas.dto.*;
import org.sas.dto.converters.SensorDataConverter;
import org.sas.entities.Sensor;
import org.sas.entities.SensorData;
import org.sas.entities.User;
import org.sas.entities.UserType;
import org.sas.enums.ErrorCode;
import org.sas.exceptions.*;
import org.sas.repositories.SensorDataRepository;
import org.sas.repositories.SensorRepository;
import org.sas.repositories.UserRepository;
import org.sas.services.JwtService;
import org.sas.utils.IPFileWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

@RestController
@RequestMapping("/api/data")
public class SensorAPIController {

    private final UserRepository userRepository;
    private final SensorRepository sensorRepository;
    private final JwtService jwtService;
    private final SensorDataRepository sensorDataRepository;

    @Autowired
    public SensorAPIController(UserRepository userRepository, SensorRepository sensorRepository,
                               JwtService jwtService, SensorDataRepository sensorDataRepository) {
        this.userRepository = userRepository;
        this.sensorRepository = sensorRepository;
        this.jwtService = jwtService;
        this.sensorDataRepository = sensorDataRepository;
    }

    @GetMapping("genToken")
    @Transactional
    @ResponseBody
    public SensorTokenDTO getSensorToken(@RequestHeader("Authorization") String accessToken) {
        User user = userRepository.findByAccessToken(accessToken).orElseThrow(() ->
                new EntityNotFoundException(ErrorCode.USER_NOT_FOUND));

        String sensorToken = jwtService.generateSensorToken(user.getLogin());
        user.setSensorToken(sensorToken);
        userRepository.save(user);

        return new SensorTokenDTO().setSensorToken(sensorToken);
    }

    @PostMapping
    @Transactional
    @ResponseBody
    public SensorDataDTOResponse saveSensorData(@RequestBody SensorDataDTO sensorDataDTO,
                                    @RequestHeader("Authorization") String sensorToken,
                                    HttpServletRequest request) {

        User user = userRepository.findBySensorToken(sensorToken).orElseThrow(() ->
                new EntityNotFoundException(ErrorCode.SENSOR_TOKEN_NOT_FOUND));

        if (Boolean.TRUE.equals(user.getIsBanned())) {
            throw new UserIsBannedException(ErrorCode.USER_IS_BANNED);
        }

        user.setRequestsNumber(user.getRequestsNumber() + 1);

        checkUserRequestsNumber(user, request);

        validateSensorDataDTO(sensorDataDTO);

        Sensor sensor = sensorRepository.findById(sensorDataDTO.getSensorId())
                .orElseThrow(() -> new EntityNotFoundException(ErrorCode.SENSOR_NOT_FOUND));

        if (!user.equals(sensor.getUser())) {
            throw new NoSuchPermissionException(ErrorCode.SENSOR_NOT_FOUND);
        }

        SensorData sensorData = SensorDataConverter.toData(sensorDataDTO, sensor);
        sensorDataRepository.save(sensorData);

        return new SensorDataDTOResponse().
                setResult(SensorDataConverter.toSensorDataDTO(sensorData));
    }

    @GetMapping("{sensorId}")
    @Transactional
    @ResponseBody
    public MultipleSensorDataDTOResponse getSensorData(
            @RequestHeader("Authorization") String accessToken,
            @PathVariable int sensorId,
            @RequestParam(name= "start") Optional<Long> startMilli,
            @RequestParam(name="end") Optional<Long> endMilli
    ) {

        User user = userRepository.findByAccessToken(accessToken).orElseThrow(() ->
                new EntityNotFoundException(ErrorCode.USER_NOT_FOUND));

        Sensor sensor = sensorRepository.findById(sensorId).orElseThrow(() ->
                new EntityNotFoundException(ErrorCode.SENSOR_NOT_FOUND));

        if (!user.equals(sensor.getUser())) {
            throw new NoSuchPermissionException(ErrorCode.SENSOR_NOT_FOUND);
        }

        ShortenedSensorDataDTO[] results;
        if (startMilli.isPresent() && endMilli.isPresent()) {
            Timestamp start = new Timestamp(startMilli.get());
            Timestamp end = new Timestamp(endMilli.get());

            results = sensorDataRepository
                    .findBySensorAndMeasurementTimeBetweenOrderByMeasurementTime(sensor, start, end)
                    .stream()
                    .map(SensorDataConverter::toShortenedSensorDataDTO)
                    .toArray(ShortenedSensorDataDTO[]::new);
        }
        else if (startMilli.isPresent()) {
            Timestamp start = new Timestamp(startMilli.get());

            results = findSensorData(sensor, start, sensorDataRepository::
                    findBySensorAndMeasurementTimeAfterOrderByMeasurementTime);
        }
        else if (endMilli.isPresent()) {
            Timestamp end = new Timestamp(endMilli.get());

            results = findSensorData(sensor, end, sensorDataRepository::
                    findBySensorAndMeasurementTimeBeforeOrderByMeasurementTime);
        }
        else {
            results = findSensorDataForDays(3, sensor);
        }

        return new MultipleSensorDataDTOResponse().setResult(results);
    }

    public ShortenedSensorDataDTO[] findSensorDataForDays(int daysNumber, Sensor sensor) {
        Timestamp daysAgo = Timestamp.from(LocalDateTime.now()
                .minusDays(daysNumber).atZone(ZoneId.systemDefault()).toInstant());

        return findSensorData(sensor, daysAgo, sensorDataRepository::
                findBySensorAndMeasurementTimeAfterOrderByMeasurementTime);
    }

    public ShortenedSensorDataDTO[] findSensorData(Sensor sensor, Timestamp daysNumber,
                                    BiFunction<Sensor, Timestamp, List<SensorData>> function) {

        return function.apply(sensor, daysNumber)
                .stream()
                .map(SensorDataConverter::toShortenedSensorDataDTO)
                .toArray(ShortenedSensorDataDTO[]::new);
    }

    public void sendMessageToUserStub(String email, String message) {
        System.out.println(message + email);
    }

    private void validateSensorDataDTO(SensorDataDTO sensorDataDTO) {
        if (sensorDataDTO.getSensorId() == null) {
            throw new EntityAttributeNotProvidedException(ErrorCode.MISSING_SENSOR_ID);
        }
        if (sensorDataDTO.getValue() == null) {
            throw new EntityAttributeNotProvidedException(ErrorCode.MISSING_SENSOR_DATA_VALUE);
        }
        if (sensorDataDTO.getMeasurementTime() == null) {
            throw new EntityAttributeNotProvidedException(ErrorCode.MISSING_SENSOR_DATA_TIMESTAMP);
        }
    }

    public void checkUserRequestsNumber(User user, HttpServletRequest request) {
        UserType userType = user.getType();

        int requestsNumber = userType.getRequestsNumber();
        int maxDailyRequestsNum = requestsNumber + userType.getRequestsReserveNumber();
        int userRequestNumber = user.getRequestsNumber();

        if (userRequestNumber == maxDailyRequestsNum) {
            sendMessageToUserStub(user.getEmail(), "ExceededRequestsNumber: ");
            throw new RequestsNumberExceededException(ErrorCode.REQUESTS_LIMIT_EXCEEDED);
        }
        else if (userRequestNumber > maxDailyRequestsNum &&
                userRequestNumber <= 2 * requestsNumber) {
            throw new RequestsNumberExceededException(ErrorCode.REQUESTS_LIMIT_EXCEEDED);
        }
        else if (userRequestNumber > 2 * requestsNumber) {
            user.setIsBanned(true);
            try (IPFileWriter writer = new IPFileWriter("ipBlackList.txt")) {
                writer.writeIP(request.getRemoteAddr() + "\n");
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
            sendMessageToUserStub(user.getEmail(), "BannedUser: ");
            throw new RequestsNumberExceededException(ErrorCode.REQUESTS_LIMIT_EXCEEDED);
        }
    }
}
