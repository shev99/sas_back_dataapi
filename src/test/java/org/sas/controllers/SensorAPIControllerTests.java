package org.sas.controllers;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
//@MockBean(name="entityManagerFactory", classes = SessionFactory.class)
public class SensorAPIControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SensorAPIController sensorAPIController;

    /**
     * be careful, this test strongly relies on a
     * sensorToken correctness (Authorization header)
     **/
    @Test
    public void contextLoads() throws Exception {
        this.mockMvc.perform(
                post("/api/data")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpc2hldjk5Iiwia" +
                                "WF0IjoxNjQwMTc0MjY3fQ.WMqcFU9iaN3l5j7QMLRPlk-3vDVTvsS7QJQzkwPyAgvrwOEt6XHgTyaFd6" +
                                "QCqa0X-FJfmlorV-b-WyA8kdSckw")
                        .content("{\"sensorId\": 1, " +
                                "\"value\": 3.12, " +
                                "\"measurementTime\": \"23.12.2021 0:20:10\"}")
                )
                .andDo(print())
                .andExpect(status().isOk());
    }
}
